export interface IBlockchainAdapter {
  blockRange: number;
  getLatestBlock(): Promise<number> | number;
  getBlock(blockNumber: number);
  getTransaction(txHash: string);
  getListTransactions(filter: any);
}
