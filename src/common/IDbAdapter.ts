export interface IDbAdapter {
  getLastScanBlock(): number | Promise<number>;
  updateLastScanBlock();
  saveBlocks(data: any);
  saveTransactions(data: any);
}
