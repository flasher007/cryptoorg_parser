import { Logger } from '@nestjs/common';
import { ClientGrpc } from '@nestjs/microservices';
import { IBlockchainAdapter } from './IBlockchainAdapter';
import { IDbAdapter } from './IDbAdapter';
import { sleep } from './utils';

export abstract class Watcher {
  protected client: any;
  protected logger: Logger;
  protected blockchainAdapter: IBlockchainAdapter;
  protected dbAdapter: IDbAdapter;

  constructor(blockchainAdapter: IBlockchainAdapter, dbAdapter: IDbAdapter) {
    this.blockchainAdapter = blockchainAdapter;
    this.dbAdapter = dbAdapter;
  }

  async run() {
    this.logger.log('Watcher start');
    let loop = true;
    while (loop) {
      try {
        await this.parser();
      } catch (e) {
        loop = false;
        this.logger.error(e);
      }
    }
  }

  async parser() {
    const latestBlock = await this.blockchainAdapter.getLatestBlock();

    const latestScanBlock = await this.dbAdapter.getLastScanBlock();

    if (
      latestScanBlock &&
      latestBlock < latestScanBlock + this.blockchainAdapter.blockRange
    ) {
      await sleep(1000);
      return;
    }

    const blockHeight = latestScanBlock ? latestScanBlock + 1 : latestBlock - 3;
    this.logger.log(`Start height = ${blockHeight}`);

    const blockData = await this.client.getBlock(blockHeight);

    await this.dbAdapter.saveBlocks(blockData);

    await this.dbAdapter.updateLastScanBlock();
  }
}
