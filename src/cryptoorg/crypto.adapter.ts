import { IBlockchainAdapter } from '../common/IBlockchainAdapter';
import { CroSDK } from '@crypto-com/chain-jslib';

export class CryptoAdapter implements IBlockchainAdapter {
  blockRange: 3;
  client: any;

  constructor() {
    this.initClient();
  }

  initClient() {}

  getBlock(blockNumber: number) {}

  getLatestBlock(): Promise<number> | number {
    return undefined;
  }

  getListTransactions(filter: any) {}

  getTransaction(txHash: string) {}
}
