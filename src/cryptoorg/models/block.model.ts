import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('crypto_blocks')
export class Block extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  timestamp: number;

  @Column({ nullable: true })
  hash: string;

  @Column()
  height: number;

  @Column({ nullable: true })
  metadata: string;
}
