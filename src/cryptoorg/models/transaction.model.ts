import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('crypto_transactions')
export class Transaction extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  block: number;

  @Column()
  blockHash: number;

  @Column({ nullable: true })
  txIndex: number;

  @Column()
  txHash: string;

  @Column({ nullable: true })
  txType: string;

  @Column({ type: 'decimal' })
  fee: number;

  @Column()
  timestamp: number;
}
