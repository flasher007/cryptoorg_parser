import { Module } from '@nestjs/common';
import { CryptoOrgService } from './cryptoorg.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Block } from './models/block.model';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { PostgresAdapter } from './postgres.adapter';
import { Transaction } from './models/transaction.model';

@Module({
  imports: [
    TypeOrmModule.forFeature([Block, Transaction]),
    // ClientsModule.register([
    //   {
    //     name: 'CRYPTO.ORG',
    //     transport: Transport.GRPC,
    //     options: {
    //       url: '',
    //       package: 'crypto',
    //       protoPath: '', // join(__dirname, 'hero/hero.proto'),
    //     },
    //   },
    // ]),
  ],
  providers: [PostgresAdapter],
})
export class CryptoOrgModule {}
