import { Injectable, Logger } from '@nestjs/common';
import { Watcher } from '../common/Watcher';

@Injectable()
export class CryptoOrgService extends Watcher {
  protected logger = new Logger(CryptoOrgService.name);
}
