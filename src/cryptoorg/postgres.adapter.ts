import { IDbAdapter } from '../common/IDbAdapter';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Block } from './models/block.model';
import { Repository } from 'typeorm';
import { Transaction } from './models/transaction.model';
import { LastBlock } from '../common/models/lastblock.model';
import { isArray } from 'util';

const blockchainType = 'crypto.org';

@Injectable()
export class PostgresAdapter implements IDbAdapter {
  constructor(
    @InjectRepository(LastBlock)
    private lastBlockRepository: Repository<LastBlock>,
    @InjectRepository(Block) private blockRepository: Repository<Block>,
    @InjectRepository(Transaction)
    private txRepository: Repository<Transaction>,
  ) {}

  async getLastScanBlock(): Promise<number> {
    const { lastBlock } = await this.lastBlockRepository.findOne({
      where: {
        type: blockchainType,
      },
    });

    return lastBlock;
  }

  async saveBlocks(data: any) {
    let blocks;
    if (Array.isArray(data)) {
    } else {
      const block = new Block();
      block.hash = data.hash;
      block.height = data.height;
      block.timestamp = data.timestamp;
      block.metadata = data.metadata;
      blocks = await this.blockRepository.save(block);
    }

    return blocks;
  }

  saveTransactions(data: any) {}

  updateLastScanBlock() {}
}
