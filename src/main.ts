import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ValidationPipe } from '@nestjs/common';
import { config as dotenvConfig } from 'dotenv';

dotenvConfig();
const RUN_PORT = +process.env.RUN_PORT || 3002;

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const cors = {
    origin: '*',
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
    preflightContinue: false,
    optionsSuccessStatus: 204,
  };

  app.enableCors(cors);
  app.useGlobalPipes(new ValidationPipe());

  await app.listen(RUN_PORT);
}
bootstrap();
