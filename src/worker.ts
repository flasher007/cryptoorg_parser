import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { config as dotenvConfig } from 'dotenv';
import { CryptoOrgService } from './cryptoorg/cryptoorg.service';
import { CryptoAdapter } from './cryptoorg/crypto.adapter';
import { PostgresAdapter } from './cryptoorg/postgres.adapter';

dotenvConfig();

async function bootstrap() {
  const app = await NestFactory.createApplicationContext(AppModule);

  const dbService = app.get(PostgresAdapter);
  const worker = new CryptoOrgService(new CryptoAdapter(), dbService);
  await worker.run();

  await app.close();
}
bootstrap();
