import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CryptoOrgModule } from './cryptoorg/cryptoorg.module';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [CryptoOrgModule, TypeOrmModule.forRoot()],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
