const options = {
  type: 'postgres',
  synchronize: true,
  host: process.env.DATABASE_HOST,
  port: process.env.DATABASE_PORT,
  username: process.env.DATABASE_USER,
  password: process.env.DATABASE_PASSWORD,
  database: process.env.DATABASE_DATABASE,
  entities: ['src/**/models/**/*.ts', 'dist/**/models/**/*'],
  logging: true,
  migrations: ['db/migration/**/*.ts'],
  cli: {
    // entitiesDir: 'src/**/entities/**/*.ts',
    migrationsDir: 'db/migration/**/*.ts',
    // subscribersDir: 'src/subscriber',
  },
  autoLoadEntities: true,
};

module.exports = options;
